import type { AppContext, AppProps } from 'next/app';
import { appWithTranslation } from 'next-i18next';
import { CssBaseline, ThemeProvider } from '@mui/material';
import theme from 'themes';

const MyApp = ({ Component, pageProps }: AppProps) => (
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <Component {...pageProps} />
  </ThemeProvider>
);

export default appWithTranslation(MyApp);
