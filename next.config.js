/** @type {import('next').NextConfig} */
const label = require('./label.json');
const { i18n } = require('./next-i18next.config');

const nextConfig = {
  experimental: {
    outputStandalone: true,
    esmExternals: false,
  },
  i18n,
  reactStrictMode: true,
  swcMinify: true,
};

module.exports = nextConfig;
