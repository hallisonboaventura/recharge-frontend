import { initReactI18next } from 'react-i18next';
import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

const namespaces = ['common'];
const supportedLngs = ['en-GB'];

const lngPathMap = {
  'en-GB': 'en-GB',
};

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    lng: 'en-GB',
    fallbackLng: 'en-GB',
    interpolation: { escapeValue: false },
    defaultNS: 'common',
    ns: namespaces,
    supportedLngs,
  });

supportedLngs.forEach((lng) =>
  namespaces.forEach((ns) =>
    i18n.addResources(
      lng,
      ns,
      require(`../public/locales/${lngPathMap[lng]}/${ns}.json`)
    )
  )
);

export { i18n };
