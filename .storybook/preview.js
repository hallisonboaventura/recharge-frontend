import { Suspense } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import MuiThemeProvider from '@mui/material/styles/ThemeProvider';
import { ThemeProvider as Emotion10ThemeProvider } from 'emotion-theming';
import { I18nextProvider } from 'react-i18next';
import theme from '../themes';
import i18n from './i18n';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

// https://github.com/mui/material-ui/blob/v5.5.2/docs/data/material/guides/migration-v4/migration-v4.md#storybook-emotion-with-v5
const withThemeProvider = (Story, context) => {
  return (
    <Emotion10ThemeProvider theme={theme}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Story {...context} />
      </MuiThemeProvider>
    </Emotion10ThemeProvider>
  );
};

const withI18nextProvider = (Story, context) => {
  return (
    <I18nextProvider i18n={i18n}>
      <Suspense fallback="Loading...">
        <Story {...context} />
      </Suspense>
    </I18nextProvider>
  );
};

export const decorators = [withI18nextProvider, withThemeProvider];
