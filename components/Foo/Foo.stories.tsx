import { ComponentStory, ComponentMeta } from '@storybook/react';
import Foo from './Foo';

export default {
  title: 'Foo',
  component: Foo,
} as ComponentMeta<typeof Foo>;

const Template: ComponentStory<typeof Foo> = (args) => (
  <Foo {...args} />
);

export const Default = Template.bind({});
Default.args = {};
