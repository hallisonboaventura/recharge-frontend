const fs = require('fs');
const label = require('./label.json');

const nextI18Next = {
  i18n: {
    defaultLocale: label.defaultLocale,
    locales: label.locales,
  },
  defaultNS: 'common',
  localeExtension: 'json',
  localePath: './public/locales',
  localeStructure: '{{lng}}/{{ns}}',
  interpolation: {
    prefix: '{{',
    suffix: '}}',
  },
};

module.exports = nextI18Next;
