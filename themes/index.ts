import { createTheme } from '@mui/material/styles';

const primary = {
  main: '#006bf5',
};

const secondary = {
  main: '#00cc00',
};

const theme = createTheme({
  palette: {
    primary,
    secondary,
  },
});

export default theme;
